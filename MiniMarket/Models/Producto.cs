﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarket.Models
{
    [Table("Producto")]
    internal class Producto
    {
        [Key]
        public int Id { get; set; }
        public string CodBarra { get; set; }
        public decimal Precio { get; set; }
        public int Stock { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Fabricante { get; set; }
        public string Categoria { get; set; }

        public Producto() {
        
        }
    }
}
