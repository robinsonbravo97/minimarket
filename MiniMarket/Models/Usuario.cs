﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarket.Models
{
    [Table("Usuario")]
    internal class Usuario
    {
        [Key]
        public int Id { get; set; }
        public string TipoUsuario { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public DateTime LastLogin { get; set; }
        public string Password { get; set; }
    }
}
