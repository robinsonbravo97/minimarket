﻿using System;
using System.Collections.Generic;
using System.Data.Entity; // genera un contexto que relaciona la base de datos con sus clases que queremos relacionar
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// creamos un "context" : Definimos la estructura de  una base de datos

namespace MiniMarket.Models
{                                                 // primero definimos y copiamos coordenadas de nuestra BD en App.config* ("Resources-App.config")
    internal class Minimarketcontext : DbContext // heredamos de una clase de entity framework, nos entrega herramientas
                                                 // para que se entienda o tenga conexion nuestra BD.
    {
        public Minimarketcontext() : base ("MinimarketCS") 
        {
        
        }

        // con el DbContext, le decimos al programa que solo queremos que estas 3 sean partes de nuestra BD
        public DbSet<Producto> Producto { get; set; }   // representa las tablas que quiero enviar y mostrar
        public DbSet<Usuario> Usuario { get; set; } // representa las tablas que quiero enviar y mostrar  
        public DbSet<Venta> Venta { get; set; } // representa las tablas que quiero enviar y mostrar 

    }
}
